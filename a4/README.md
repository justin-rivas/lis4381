> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Justin Rivas

### Assignment 4 Requirements:

1. Set-up cloned files for A4
2. Add jQuery validation and regular expressions
3. A4 Questions
4. A4 index.jsp screenshot
5. Skillsets 10-12 screenshots

#### README.md file should include the following items:

* Screenshot of A4 index.jsp
* Screenshot of Failed and Successful jQuery Validations
* Screenshot of Main page for A4
* Screenshot of Skillsets


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
>

#### Assignment Screenshots:

*Screenshot of a4/index.jsp http://localhost:9999/lis4381/a4/index.jsp

![A4/index.jsp Main Page Screenshot](img/mainpage.png)

*First Screenshot of Failed Validation - A4*:

![First Failed Validation Screenshot](img/validationfail1.png)

*Second Screenshot of Failed Validation - A4*:

![Second Failed Validation Screenshot](img/validationfail2.png)

*Screenshot of Successful Validation - A4*:

![Successful Validation Screenshot](img/validationcheck.png)

*Screenshot of Skillset 10*:

![Skillset 10 Screenshot](img/Q10screenshot.png)

*Screenshot of Skillset 11*:

![Skillset 11 Screenshot](img/Q11screenshot.png)

*Screenshot of Skillset 12*:

![Skillset 12 Screenshot](img/Q12screenshot.png)

#### Document Links

*Link to A4/index.jsp*:
[a4](index.jsp "Link to A4 index.jsp")


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/justin-rivas/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/justin-rivas/myteamquotes/ "My Team Quotes Tutorial")
