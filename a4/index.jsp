<!DOCTYPE html>
<html lang="en">
<head>
<!--
"Time-stamp: <Sat, 12-19-20, 17:39:11 Eastern Standard Time>"
//-->
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
    <meta name="author" content="Justin Rivas">
    <link rel="icon" href="favicon.ico">

    <title>LIS 4381 - Assignment 4</title>

    <%@ include file="/css/include_css.jsp" %>      

</head>
<body>

<!-- display application path -->
<% //= request.getContextPath()%>
   
<!-- can also find path like this...<a href="${pageContext.request.contextPath}${'/a5/index.jsp'}">A5</a> -->

    <%@ include file="/global/nav.jsp" %>  

    <div class="container">
        <div class="starter-template">
                    <div class="page-header">
                        <%@ include file="global/header.jsp" %>
                    </div>
                   
                    <h2>Customers</h2>

                    <form id="add_customer_form" method="post" class="form-horizontal" action="#">

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Name:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" maxlength="15" name="name" />
                            </div>
                        </div>                     

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Street:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" maxlength="30" name="street" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">City:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" maxlength="30" name="city" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">State:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" maxlength="2" name="state" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Zip:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" maxlength="9" name="zip" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Phone:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" maxlength="10" name="phone" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Email:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" maxlength="100" name="email" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">URL</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" maxlength="100" name="url" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">YTD_Sales:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" maxlength="11" name="ytd_sales" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Notes:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" maxlength="255" name="notes" />
                            </div>
                        </div>
                       
                        <div class="form-group">
                            <div class="col-sm-6 col-sm-offset-3">
                                <button type="submit" class="btn btn-primary" name="signup" value="Sign up">Add</button>
                            </div>
                        </div>
                    </form>

    <%@ include file="/global/footer.jsp" %>

    </div> <!-- end starter-template -->
 </div> <!-- end container -->

    <%@ include file="/js/include_js.jsp" %>        
 
<script type="text/javascript">
$(document).ready(function() {

    $('#add_customer_form').formValidation({
            message: 'This value is not valid',
            icon: {
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
            },
            fields: {

                name: {
                            validators: {
                                    notEmpty: {
                                            message: 'Name required'
                                    },
                                    stringLength: {
                                            min: 1,
                                            max: 15,
                                            message: 'Name no more than 15 characters'
                                    },
                                    regexp: {
                                        //http://www.regular-expressions.info/
                                        //http://www.regular-expressions.info/quickstart.html
                                        //http://www.regular-expressions.info/shorthand.html
                                        //http://stackoverflow.com/questions/13283470/regex-for-allowing-alphanumeric-and-space
                                        //alphanumeric (also, "+" prevents empty strings):
                                        regexp: /^[a-zA-Z0-9\-\_]+$/,
                                        message: 'Name can only contain letters, numbers, hyphens, and underscores.'
                                    },                                  
                            },
                    },

                lname: {
                            validators: {
                                    notEmpty: {
                                            message: 'Last name required'
                                    },
                                    stringLength: {
                                            min: 1,
                                            max: 30,
                                            message: 'Last name no more than 30 characters'
                                    },
                                    regexp: {
                                        regexp: /^[a-zA-Z\-]+$/,
                                        message: 'Last name can only contain letters and hyphens'
                                    },                                  
                            },
                    },
                    street: {
                            validators: {
                                    notEmpty: {
                                            message: 'Street required'
                                    },
                                    stringLength: {
                                            min: 1,
                                            max: 30,
                                            message: 'Street no more than 30 characters'
                                    },
                                    regexp: {
                                        regexp: /^[a-zA-Z0-9,\-\s\.]+$/,
                                        message: 'Street can only contain letters and hyphens'
                                    },                                  
                            },
                    },

                    city: {
                            validators: {
                                    notEmpty: {
                                            message: 'City required'
                                    },
                                    stringLength: {
                                            min: 1,
                                            max: 30,
                                            message: 'City no more than 30 characters'
                                    },
                                    regexp: {
                                        regexp: /^[a-zA-Z\-\s]+$/,
                                        message: 'City can only contain letters and hyphens'
                                    },                                  
                            },
                    },

                    state: {
                            validators: {
                                    notEmpty: {
                                            message: 'State required'
                                    },
                                    stringLength: {
                                            min: 2,
                                            max: 2,
                                            message: 'State must be 2 characters'
                                    },
                                    regexp: {
                                        regexp: /^[a-zA-Z]+$/,
                                        message: 'State can only contain letters'
                                    },                                  
                            },
                    },

                    zip: {
                            validators: {
                                    notEmpty: {
                                            message: 'Zip required'
                                    },
                                    stringLength: {
                                            min: 5,
                                            max: 9,
                                            message: 'Zip must be 5-9 digits'
                                    },
                                    regexp: {
                                        regexp: /^[0-9]+$/,
                                        message: 'Zip only contains numbers'
                                    },                                  
                            },
                    },

                    phone: {
                            validators: {
                                    notEmpty: {
                                            message: 'Phone required'
                                    },
                                    stringLength: {
                                            min: 10,
                                            max: 10,
                                            message: 'Phone must be 10 digits'
                                    },
                                    regexp: {
                                        regexp: /^[0-9]+$/,
                                        message: 'Zip only contains numbers'
                                    },                                  
                            },
                    },

                    email: {
                            validators: {
                                    notEmpty: {
                                            message: 'Email address is required'
                                    },

                                    /*
                                    //built-in e-mail validator, comes with formValidation.min.js
                                    //using regexp instead (below)
                                    emailAddress: {
                                            message: 'Must include valid email address'
                                    },
                                    */
                               
                                    stringLength: {
                                            min: 1,
                                            max: 100,
                                            message: 'Email no more than 100 characters'
                                    },
                                    regexp: {
                                    regexp: /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/,
                                        message: 'Must include valid email'
                                    },                                                                      
                            },
                    },

                    url: {
                            validators: {
                                    notEmpty: {
                                            message: 'URL required'
                                    },
                                    stringLength: {
                                            min: 1,
                                            max: 100,
                                            message: 'URL must be no more than 100 characters'
                                    },
                                    regexp: {
                                        regexp: /^[a-zA-Z0-9\.\/\:]+$/,
                                        message: 'URL contains only numbers and decimal point'
                                    },                                  
                            },
                    },

                    ytd_sales: {
                            validators: {
                                    notEmpty: {
                                            message: 'YTD Sales required'
                                    },
                                    stringLength: {
                                            min: 1,
                                            max: 11,
                                            message: 'YTD Sales must be no more than 10 digits including decimal point'
                                    },
                                    regexp: {
                                        regexp: /^[0-9\.]+$/,
                                        message: 'Balance contains only numbers and decimal point'
                                    },                                  
                            },
                    },
            }
    });
});
</script>

</body>
</html>