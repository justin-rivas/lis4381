import java.util.Scanner;

public class Methods {
    public static void getRequirements() {
        System.out.println("Developer: Justin Rivas");
        System.out.println("Program evaluates integers as either even or odd");
        System.out.println("Note: Program does not check for non-numeric characters");
        System.out.println();
    }

    public static void evaluateNumber() {
        int num = 0;
        System.out.print("Enter integer: ");
        Scanner sc = new Scanner(System.in);
        num = sc.nextInt();
        if (num % 2 == 0) {
            System.out.println(num + " is an even number.");
        } else {
            System.out.println(num + " is an odd number.");
        }
    }
}