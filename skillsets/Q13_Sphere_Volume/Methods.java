import java.util.Scanner;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.math.BigDecimal;
import java.math.RoundingMode;
import javax.lang.model.util.ElementScanner14;
import java.util.Arrays;
import java.util.ArrayList;

public class Methods 
{

    public static void getRequirements()
    {
        System.out.println("Developer: Justin Rivas");
        System.out.println("Program calculates sphere volume in liquid U.S. gallons from user entered diameter in inches.");
        System.out.println("Program continues to prompt user for entry until no longer requested.");
    }

    public static int getInt(String whichNum)
    {
        //initialize objects
        Scanner scnr = new Scanner(System.in);
        int num = 0;

        //prompt user
        System.out.print("Please enter " + whichNum + ": ");

        //validation
        while (!scnr.hasNextInt())
        {
            System.out.println("Not a valid interger!");
            scnr.next();
            System.out.print("Please enter a valid interger: ");
        }
        System.out.println("");
        num = scnr.nextInt();

        return num;
    }

    public static void getSphereVolume()
    {
        //init variables
        int diameter = 0;
        double volume = 0.0;
        double gallons = 0.0;
        char choice = ' ';
        Scanner scnr = new Scanner(System.in);
        
        do
        {
        diameter = getInt("diameter in inches");

        volume = ((4.0/3.0) * Math.PI * Math.pow(diameter/2.0, 3));
        gallons = volume/231;
        System.out.println("Sphere volume: " + String.format("%,.2f", gallons) + " liquid U.S. gallons.");

        System.out.print("\nDo you want to calulate another sphere volume (y or n)?");
        choice = scnr.next().charAt(0);
        choice = Character.toLowerCase(choice);
        }while (choice == 'y');

        System.out.println("Thank you for using our sphere volume calculator.");
    }
}
