> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 4381 - Mobile Application Development

## Justin Rivas

### LIS 4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Install JDK
    - Install Visual Studio Code
    - Install Android Studio
    - Provide Installation Screenshots
    - Create "My First App"
    - Complete Bitbucket Tutorial

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create Healthy Recipes Application via Android Studio
    - Provide screenshots of both complete interfaces for the app
    - Provide screenshots of Skillsets 1-3

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Screenshot of Entity Relationship Diagram
    - Screenshot of running application's opening user interface
    - Screenshot of running application’s processing user input
    - Screenshots of 10 records for each table — use select * from each table
    - Links to the following files:
        - a3.mwb
        - a3.sql
    - Skillset 4: Decisions Structures Application
        - Screenshots of Skillset 4 running
    - Skillset 5: Random Number Generator Application
        - Screenshots of Skillset 5 running
    - Skillset 6: Methods Application
        - Screenshots of Skillset 6 running

4. [P1 README.md](P1/README.md "My P1 README.md file")
    - Screenshot of running application's opening user interface
    - Screenshot of running application’s second user interface "Details"
    - Skillset 7: Random Number Generator Data Validation
    - Skillset 8: Largest Three Numbers
    - Skillset 9: Array Runtime Data Validation

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Set-up cloned files for A4
    - Add jQuery validation and regular expressions
    - A4 Questions
    - A4 index.jsp screenshot
    - Skillsets 10-12 screenshots

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Develop a web based data entry form that displays the sql data table.
    - Use jQuery client based data validation to avoid SQL injection as well as server side data validation.
    - Screenshot of invalid data and valid data.
    - Finish skillsets 13-15.


