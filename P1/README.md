# LIS4381 - Mobile Web Application Development

## Justin Rivas

### P1 Requirements

*Parts:*

1. Create a launcher icon image and display it in both activities (screens) 
2. Must add background color(s) to both activities 
3. Must add border around image and button 
4. Must add text shadow (button)

#### README.md file should include the following items

   - Screenshot of running application's opening user interface
   - Screenshot of running application’s second user interface "Details"
   - Skillset 7: Random Number Generator Data Validation
   - Skillset 8: Largest Three Numbers
   - Skillset 9: Array Runtime Data Validation
  

<br>

<!--
>> This is a blockquote.
>
> This is the second paragraph in the blockquote.
>
-->

#### Assignment Screenshots

| *Screenshot of Application's Opening Interface*:  | *Screenshot of Application's First Processing Interface*:  | 
|-----------------------------------|------------------------------------|
| ![My Business Card Application Opening Interface Screenshot](img/mybusinesscard1.png "Screenshot of My Business Card Application Details Interface")  | ![My Business Card - Details Interface](img/mybusinesscard2.png "Screenshot Interface for My Event")  |

<br>

<br>

#### Java Skillsets

*Screenshot of Skillset 7 - Random Number Generator Data Validation*:

![Skillset 7 Screenshot](img/Q7screenshot.png)

*Screenshot of Skillset 8 - Largest Three Numbers*:

![Skillset 8 Screenshot](img/Q8screenshot.png)

*Screenshot of Skillset 9 - Array Runtime Data Validation*:

![Skillset 9 Screenshot](img/Q9screenshot.png)

<br>




