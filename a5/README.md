

# LIS 4381

## Justin Rivas

> ### Assignment #5 Requirements:

1. Develop a web based data entry form that displays the sql data table.
2. Use jQuery client based data validation to avoid SQL injection as well as server side data validation.
3. Finish skillsets 13-15.

> #### README.md file should include the following items:

* Screenshot of A5 index.
* Screenshot of invalid data with error message.
* Screenshot of valid data with new record.
* Screenshots of Skillsets.

> #### Assignment Screenshots:

### A5 Index
![A5 Index](img/.png)

### Valid Data Entry
Valid Data                                  |New Record                               
--------------------------------------------|------------------------------------------------
![Valid Data ](img/.png)          |![New Record](img/.png)            

### Invalid Data Entry
Invalid Data                                  |Error Message                              
----------------------------------------------|------------------------------------------------
![Invalid Data ](img/.png)          |![Error Message](img/.png)  

> #### *Assignment Skillsets*:

Simple Calculator PHP             |Read/Write Data PHP
----------------------------------|-----------------------------
![skillset 14](img/.gif)  |![skillset 12](img/.gif)

### Volume of a Sphere Calculator
![skillset 13](img/Q13screenshot.png)  
