> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 - Mobile Application Development

## Justin Rivas

### Assignment 2 Requirements:


1. Android Studio and Java application: "Healthy Recipes"
2. Screenshots of skillsets 1-3 compiled in Terminal


#### README.md file should include the following items:

* Screenshot of Android Studio app's first interface
* Screenshot of Android Studio app's second interface
* Screenshot of Skillset 1, 2, & 3


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> 

#### Assignment Screenshots:

*Screenshot of First Interface*:

![First Project Interface Screenshot](img/recipescreenshot1.png "Screenshot of the first interface for the Healthy Recipes application")

*Screenshot of Second Interface*:

![Second Project Interface Screenshot](img/recipescreenshot2.png "Screenshot of the second interface for the Healthy Recipes application")

*Screenshot of Skillset 1 - Even or Odd*:

![Skillset 1 Screenshot](img/Q1.png)

*Screenshot of Skillset 2 - Largest Number*:

![Skillset 2 Screenshot](img/Q2.png)

*Screenshot of Skillset 3 - Arrays and Loops*:

![Skillset 3 Screenshot](img/Q3.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/justin-rivas/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/justin-rivas/myteamquotes/ "My Team Quotes Tutorial")
