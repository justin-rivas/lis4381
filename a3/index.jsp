<!DOCTYPE html>
<html lang="en">
<head>
<!--
"Time-stamp: <Sat, 01-02-21, 18:35:01 Eastern Standard Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Justin Rivas">
	<link rel="icon" href="favicon.ico">

	<title>LIS4381 - Assignment3</title>

	<%@ include file="/css/include_css.jsp" %>		
	
</head>
<body>

<!-- display application path -->
<% //= request.getContextPath()%>


	
<!-- can also find path like this...<a href="${pageContext.request.contextPath}${'/a5/index.jsp'}">A5</a> -->

	<%@ include file="/global/nav.jsp" %>	

	<div class="container">
		<div class="starter-template">
					<div class="page-header">
						<%@ include file="global/header.jsp" %>
					</div>
					<p>Requirements: Frequently, not only will you be asked to design and develop Web applications, but you will also be asked to create (design) database solutions that interact with the client/server (e.g.. web) application-and, in fact, the data repository is the core of all Web applications. Hence, the following business requirements.</p>
					<p>A pet store owner, who owns a number of pet stores, requests that you develop a Web application whereby he and his team can record, track, and maintain relevant company data, based upon the following business rules:</p>
					<ol>
						<li>A customer can buy many pets, but each pet, if purchased, is purchased by only one customer.</li>
						<li>A store has many pets, but each pet is sold by only one store.</li>
					</ol>
					<p>For the Pet's R-Us business, it's important to ask the following question to get a better idea of how the database and Web Application should work together:
						<ul>
							<li>Can a customer exist without a pet? Seems reasonable. Yes. (optional)</li>
							<li>Can a pet exist without a customer? Again, yes. (optional)</li>
							<li>Can a pet store not have any pets? It wouldn't be a pet store. (mandatory)</li>
							<li>Can a pet exist without a pet store? Not in this design. (mandatory)</li>
						</ul>
						<p>
							Solutions:
							<li><a href="A3.mwb" target="_blank">A3.mwb</a></li>
							<li><a href="a3.sql" target="_blank">A3.sql</a></li>
							
						</p>
						
					</p>
				

					<h4>Petstore ERD:</h4>
					<img src="img/ERDscreenshot.png" class="img-responsive center-block" alt="Petstore ERD" />

					<br /> <br />
					<b>My Event - Activity 1:</b><br />
					<img src="img/ticketscreenshot1.png" class="img-responsive center-block" alt="My Event Screenshot 1" />

					<br /> <br />
					<b>My Event - Activity 2:</b><br />
					<img src="img/ticketscreenshot2.png" class="img-responsive center-block" alt="My Event Screenshot 2" />

	<%@ include file="/global/footer.jsp" %>

	</div> <!-- end starter-template -->
 </div> <!-- end container -->

 	<%@ include file="/js/include_js.jsp" %>		

</body>
</html>
